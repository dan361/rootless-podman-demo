rootless-podman-demo
=========

A simple NGINX container in podman running rootless.

Requirements
------------
ansible installed locally

Role Variables
--------------

In vars/main.yml you can set a volume path for the nginx container and your non-root podman_user.


Example usage
--------------

```
ansible-playbook main.yml -K
```
to setup the host and deploy the container.

Cleanup
-------
```
ansible-playbook main.yml -K --tags cleanup
```
to clean up the environment.


Example Playbook
----------------

see main.yml

License
-------

GPL

Author Information
------------------

Daniel Mötefindt
daniel@m8t.io
